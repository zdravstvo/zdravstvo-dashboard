import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class LimesService {

  activeData;
  keys;

  constructor(private http: HttpClient, private router: Router) { }

  getData(table: string) {
    this.http.get('http://localhost:8080/limes-api/'.concat(table).concat('/getAll'),
      {headers: {'Content-Type': 'application/json'}})
      .subscribe(data => {
        this.keys = Object.keys(data[0]);
        this.activeData = data;
      });
  }

  paramSearch(table:string, json: any) {
    this.http.post('http://localhost:8080/limes-api/'.concat(table).concat('/params'),JSON.stringify(json),
      {headers: {'Content-Type': 'application/json'}}) .subscribe(data => {
      this.activeData = data;
    });
  }

  getReport(table:string, json: any) {
    this.http.post('http://localhost:8000/5b1eca2ead8b90000b15125a/limes-api/'.concat(table).concat('/report'), JSON.stringify(json),
      {headers: {'Content-Type': 'application/json', 'Authorization':sessionStorage.getItem('token')}}).subscribe( data => {
        this.keys = Object.keys(data[0]);
        this.activeData = data;
    });
  }

}
