import { Component, OnInit } from '@angular/core';
import {LimesService} from "./limes.service";
declare var $: any;

@Component({
  selector: 'app-limes',
  templateUrl: './limes.component.html',
  styleUrls: ['./limes.component.css']
})
export class LimesComponent implements OnInit {

  name: string;
  table: string;
  pageOfItems: Array<any>;

  constructor(private limesService: LimesService) { }

  ngOnInit() {
  }

  select(table: string, name: string) {
    $('#controls').css('visibility', 'visible');
    this.name = name;
    this.table = table;
    if (name === 'Dozvole' || name === 'Promotivni materijal') {
      $('#report').css('visibility', 'visible');
      $('#fromDate').css('visibility', 'visible');
      $('#toDate').css('visibility', 'visible');
    } else {
      $('#report').css('visibility', 'hidden');
      $('#fromDate').css('visibility', 'hidden');
      $('#toDate').css('visibility', 'hidden');
    }
    this.getAll();
  }

  getAll() {
    this.limesService.getData(this.table);
  }

  paramSearch() {
    const dropdown = document.getElementById('searchTypeButton') as HTMLInputElement;
    const searchField = document.getElementById('searchField') as HTMLInputElement;
    const keyString = dropdown.innerText.trim();
    const json = {'parameterMap': { [keyString] : searchField.value.trim()}};
    this.limesService.paramSearch(this.table, json);
  }

  setLoginType(name: String) {
    $('#searchTypeButton').html(name + ' <span class=\"caret\"></span>');
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }

  getReport() {
    const fromDate = document.getElementById('fromDate') as HTMLInputElement;
    const toDate = document.getElementById('toDate') as HTMLInputElement;
    const json = {'toDate': toDate.value, 'fromDate': fromDate.value};
    this.limesService.getReport(this.table, json);
  }

}
