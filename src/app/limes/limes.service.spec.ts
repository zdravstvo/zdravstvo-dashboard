import { TestBed, inject } from '@angular/core/testing';

import { LimesService } from './limes.service';

describe('LimesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LimesService]
    });
  });

  it('should be created', inject([LimesService], (service: LimesService) => {
    expect(service).toBeTruthy();
  }));
});
