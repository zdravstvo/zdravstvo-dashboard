import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  currentUser: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.currentUser = sessionStorage.getItem('user');
  }

  navigate(url) {
    this.router.navigate(['/dashboard', {outlets: {'dashboard': [url]}}]);
  }

}
