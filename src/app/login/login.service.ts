import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  login(json: any) {
    console.log(json);
    this.http.post('http://localhost:8000/users/login', JSON.stringify(json),
      {headers: {'Content-Type': 'application/json'}})
      .subscribe(data => {
        if (data['status'] !== 'ERROR') {
          console.log(data['result']['token']);
          sessionStorage.setItem('user', json['email']);
          sessionStorage.setItem('token', data['result']['token']);
          this.router.navigate(['dashboard']);
        } else {
          alert(data['message']);
        }
      });
  }

}
